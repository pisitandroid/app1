package com.pisit.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.pisit.app1.databinding.ActivityHello2Binding

class HelloActivity2 : AppCompatActivity() {
    private lateinit var binding: com.pisit.app1.databinding.ActivityHello2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello2)
        binding = ActivityHello2Binding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val name = intent?.extras?.getSerializable("name").toString()
        binding.textView5.text = name
    }
}