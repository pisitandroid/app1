package com.pisit.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.pisit.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        setContentView(R.layout.activity_main)
        val view = binding.root
        setContentView(view)
        findViewById<Button>(R.id.button).setOnClickListener {
            val intent = Intent(this, HelloActivity2::class.java)
            val name = binding.textView.text.toString()
            Log.d("name", name)
            intent.putExtra("name", name)
            this.startActivity(intent)
        }
    }
}